FROM node:18-alpine

RUN apk update && \
    apk upgrade && \
    apk add tini

WORKDIR /usr/node/app

COPY --chown=node:node . /usr/node/app

RUN npm ci --quiet --production
RUN npm install -g nodemon

USER node

ARG GIT_HASH
ENV GIT_HASH $GIT_HASH

EXPOSE 3000

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/node" ]
CMD ["index.js"]