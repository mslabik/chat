import { ObjectId } from "mongoose";
import { User, Room, Message } from './models.js';

export default async function createRoutes(app) {

    // users
    app.get('/users', () => {
        return User.find();
    });

    app.post('/users', (req) => {
        const user = new User(req.body);
        return user.save()
    });

    app.delete('/users/:userId', (req) => {
        const { userId } = req.params;
        return User.deleteOne({ _id: userId })
    });

    // messages
    app.get('/messages/:roomId', (req) => {
        const { roomId } = req.params;
        return Message.find({
            roomId
        }).populate(['roomId', 'userId']);
    });

    app.post('/messages/:roomId', (req) => {
        const { text, userId } = req.body;
        const { roomId } = req.params
        const message = new Message({
            text, userId, roomId
        });
        return message.save()
    });

    // rooms
    app.get('/rooms', () => {
        return Room.find()
    });

    app.post('/rooms', (req) => {
        const room = new Room(req.body);
        return room.save()
    });

    app.put('/rooms/:roomId/users', (req) => {
        const { userId } = req.body;
        const { roomId } = req.params;
        return Room.updateOne({ _id: roomId },  { $addToSet: { userIds: userId } })
    });

    app.delete('/rooms/:roomId/users', (req) => {
        const { userId } = req.body;
        const { roomId } = req.params;
        return Room.updateOne({ _id: roomId },  { $pull: { userIds: userId } })
    });

    app.delete('/rooms/:roomId', (req) => {
        const { roomId } = req.params;
        return Room.deleteOne({ _id: roomId })
    });
}
