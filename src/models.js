import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
})
const roomSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    userIds: {
        type: [{ type: mongoose.Types.ObjectId, ref: 'user'}],
        default: [],
    }
})
const messageSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
    },
    roomId: {
        type: mongoose.Types.ObjectId,
        ref: 'room',
        required: true
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'user',
        required: true
    }
}, { timestamps: true })

messageSchema.path('userId').validate(async (id) => User.findById(id), 'User does not exist');
messageSchema.path('roomId').validate(async (id) => Room.findById(id), 'Room does not exist');

export const User = mongoose.model('user', userSchema);
export const Room = mongoose.model('room', roomSchema);
export const Message = mongoose.model('message', messageSchema);
