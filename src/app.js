import fastify from 'fastify';
import createRoutes from './routes.js';
import mongoose from 'mongoose';

export default async function createApp() {
  const app = fastify();
  const mongodb = await mongoose.connect('mongodb://usr:pwd@mongodb:27017')

  app.register(createRoutes, { mongodb });
  await app.ready();
  return app;
}
