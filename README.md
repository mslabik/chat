### Running the app
Tun run the application run docker-compose up, it will create a service and a mongoDB container
### Endpoints
- GET /users - list all users
- POST /users - create a user
- DELETE /users/:userId - remove the given user
- GET /messages/:roomId - get all the messages from the room
- POST /messages/:roomId - create a message in the room
- GET /rooms - list all rooms
- POST /rooms - create a room
- PUT /rooms/:roomId/users - add a user to the room
- DELETE /rooms/:roomId/users - remove a user from the room
- DELETE /rooms/:roomId - remove a room
