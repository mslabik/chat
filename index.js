import createApp from './src/app.js';

const PORT = 3000;

function registerSignalHandlers(app) {
  function shutdown() {
    app.log.info({ message: 'Gracefully stopping server' });
    app.close(() => {
      process.exit(0);
    });
  }
  process.on('SIGTERM', shutdown);
  process.on('SIGINT', shutdown);
}

function registerGracefulShutdown(app) {
  process.on('uncaughtException', (err) => {
    app.log.error({
      message: `Uncaught exception: ${err.message}, shutting down the server`,
      err,
    });
    app.close(() => {
      process.exit(1);
    });
  });
}

async function start() {
  try {
    const app = await createApp();

    registerGracefulShutdown(app);
    registerSignalHandlers(app);

    await app.listen(PORT, '::');
  } catch (err) {
    app.log.error({
      message: `Uncaught exception: ${err.message}, shutting down the server`,
      err,
    });
    setImmediate(() => {
      throw err;
    });
  }
}

start();
